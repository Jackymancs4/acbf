"""Portability functions for ACBFV.

Copyright (C) 2011-2014 Robert Pastierovic
https://launchpad.net/~just-me
"""

# -------------------------------------------------------------------------
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as published
# by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# -------------------------------------------------------------------------


import os
import sys
import tempfile


def get_home_directory():
    """On UNIX-like systems, this method will return the path of the home
    directory, e.g. /home/username. On Windows, it will return a ACBFV
    sub-directory of <Documents and Settings/Username>.
    """
    if sys.platform == 'win32':
        return unicode(os.path.join(os.path.expanduser('~'), 'acbfv'))
    else:
        return unicode(os.path.expanduser('~'))


def get_config_directory():
    """Return the path to the ACBFV config directory. On UNIX, this will
    be $XDG_CONFIG_HOME/acbfv, on Windows it will be the same directory as
    get_home_directory().
    
    See http://standards.freedesktop.org/basedir-spec/latest/ for more
    information on the $XDG_CONFIG_HOME environmental variable.
    """
    if sys.platform == 'win32':
        return os.path.join(os.path.expanduser('~'), 'acbfv_conf')
    else:
        base_path = os.getenv('XDG_CONFIG_HOME',
            os.path.join(get_home_directory(), '.config'))
        return os.path.join(base_path, 'acbfv')


def get_data_directory():
    """Return the path to the ACBFV data directory. On UNIX, this will
    be /tmp/acbfv, on Windows it will be
    C:\Documents and settings\[user]\Application settings\Temp\acbfv.
    """

    return unicode(os.path.join(tempfile.gettempdir(), 'acbfv'))

def get_fonts_directory():
    if sys.platform == 'win32':
        if os.getenv('CSIDL_FONTS') != None:
          return os.getenv('CSIDL_FONTS')
        else:
          return 'C:\Windows\Fonts'
    elif sys.platform.startswith('linux'):
        if os.path.isdir('/usr/share/fonts'):
          return '/usr/share/fonts'
        else:
          return '/system/fonts'

def get_platform():
    return sys.platform

