from distutils.core import setup
import py2exe
import glob
import numpy
import os
import sys

# add any numpy directory containing a dll file to sys.path
def numpy_dll_paths_fix():
    paths = set()
    np_path = numpy.__path__[0]
    for dirpath, _, filenames in os.walk(np_path):
        for item in filenames:
            if item.endswith('.dll'):
                paths.add(dirpath)

    sys.path.append(*list(paths))

numpy_dll_paths_fix()

setup(
    name = 'ACBF Editor',
    description = 'Editor for ACBF documents',
    version = '0.91',

    windows = [
                  {
                      'script': 'acbfe.py',
                      'icon_resources': [(1, '../images/acbfe.ico')],
                  }
              ],

    options = {
                  'py2exe': {
                      'packages': 'encodings, lxml, gzip, PIL, gtk, patoolib, cv2',
                      'includes': 'cairo, pango, pangocairo, atk, gobject, gio, gtk.keysyms, matplotlib.figure, pylab, numpy, matplotlib.backends.backend_gtkagg, matplotlib.backends.backend_tkagg',
                      'excludes': '_tkagg, _agg2, _cocoaagg, _fltkagg, _gtkcairo, _cairo',
                      'dll_excludes': ['libglade-2.0-0.dll','MSVCP90.dll','api-ms-win-core-string-l1-1-0.dll','api-ms-win-core-registry-l1-1-0.dll','api-ms-win-core-errorhandling-l1-1-1.dll','api-ms-win-core-string-l2-1-0.dll','api-ms-win-core-profile-l1-1-0.dll','api-ms-win*.dll','api-ms-win-core-processthreads-l1-1-2.dll','api-ms-win-core-libraryloader-l1-2-1.dll','api-ms-win-core-file-l1-2-1.dll','api-ms-win-security-base-l1-2-0.dll','api-ms-win-eventing-provider-l1-1-0.dll','api-ms-win-core-heap-l2-1-0.dll','api-ms-win-core-libraryloader-l1-2-0.dll','api-ms-win-core-localization-l1-2-1.dll','api-ms-win-core-sysinfo-l1-2-1.dll','api-ms-win-core-synch-l1-2-0.dll','api-ms-win-core-heap-l1-2-0.dll','api-ms-win-core-handle-l1-1-0.dll','api-ms-win-core-io-l1-1-1.dll','api-ms-win-core-com-l1-1-1.dll','api-ms-win-core-memory-l1-1-2.dll','api-ms-win-core-version-l1-1-1.dll','api-ms-win-core-version-l1-1-0.dll']
                  }
              },

    data_files = [(r'mpl-data', glob.glob('C:\Python27\Lib\site-packages\matplotlib\mpl-data\*.*')),
                  (r'mpl-data', [r'C:\Python27\Lib\site-packages\matplotlib\mpl-data\matplotlibrc']),
                  (r'mpl-data\images',glob.glob(r'C:\Python27\Lib\site-packages\matplotlib\mpl-data\images\*.*')),
                  (r'mpl-data\stylelib',glob.glob(r'C:\Python27\Lib\site-packages\matplotlib\mpl-data\stylelib\*.*')),
                  (r'mpl-data\fonts',glob.glob(r'C:\Python27\Lib\site-packages\matplotlib\mpl-data\fonts\*.*'))]
)
