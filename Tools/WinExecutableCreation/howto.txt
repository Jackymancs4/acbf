1. you should have installed Python and all required libraries already:
  Python - http://www.python.org/download/releases/2.7.5/
  PyGtk all in one installer - http://ftp.gnome.org/pub/GNOME/binaries/win32/pygtk/2.24/
  lxml - https://pypi.python.org/pypi/lxml
  PIL with libfreetype - http://www.lfd.uci.edu/~gohlke/pythonlibs/#pil
  Matplotlib - http://www.lfd.uci.edu/~gohlke/pythonlibs/#matplotlib
  Six - http://www.lfd.uci.edu/~gohlke/pythonlibs/#six
  Python-dateutil - http://www.lfd.uci.edu/~gohlke/pythonlibs/#python-dateutil
  Pyparsing - http://www.lfd.uci.edu/~gohlke/pythonlibs/#pyparsing
  Numpy - http://www.lfd.uci.edu/~gohlke/pythonlibs/#numpy
  Pysqlite - http://www.lfd.uci.edu/~gohlke/pythonlibs/#pysqlite
  sqlite DLL from http://sqlite.org/download.html
  OpenCV - http://www.lfd.uci.edu/~gohlke/pythonlibs/#opencv
2. download and install py2exe: http://sourceforge.net/projects/py2exe/files/
3. copy setup.py from this directory to ACBF Viewer "src" directory
4. run from console: C:\Python27\python.exe setup.py py2exe
5. executable is created inside the "dist" directory
6. copy to "dist" directory all directories from this directory: etc, lib, share
7. copy to "dist" directory "images" directory from ACBF Viewer directory
8. you may rename the "dist" directory to "ACBF Viewer" then run acbfv.exe from inside
