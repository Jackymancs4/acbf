/****************************************************************************
 *
 *  mkacbf, an ACBF comic book file creation utility.
 *  Copyright (C) 2012  Emery Hemingway
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>

// getopt
#include <unistd.h>
#include <getopt.h>

#include <libxml/xmlwriter.h>

#include "config.h"

#ifdef HAS_LIBUUID
#include <uuid.h>
#endif

#define SCHEMA_NAMESPACE "http://www.fictionbook-lib.org/xml/acbf/1.0"
#define BUFFER_SIZE 128


static struct option long_options[] = {
    {"version",   no_argument, NULL, 'v'},
    {"help",      no_argument, NULL, 'h'},
    {"force",     no_argument, NULL, 'F'},
    /* {"no-indent", no_argument, NULL, 'I'}, */
    {"title",     required_argument, 0, 't'},
    {"sequence",  required_argument, 0, 'S'},
    {"number",    required_argument, 0, 'N'},
    {"first",     required_argument, 0, 'f'},
    {"middle",    required_argument, 0, 'm'},
    {"last",      required_argument, 0, 'l'},
    {"nick",      required_argument, 0, 'n'},
    {"homepage",  required_argument, 0, 'w'},
    {"email",     required_argument, 0, 'e'},
    {"source",    required_argument, 0, 's'},
    {"ver",       required_argument, 0, 'V'},
    {"history",   required_argument, 0, 'H'},
    {0, 0, 0, 0}
};

static void
printUsage()
{
  printf("Usage: %s [OPTION]... COVER_FILE PAGE_FILE... ACBF_FILE\n", PACKAGE_NAME);
  printf("Create ACBF_FILE from COVER_FILE and PAGE_FILE(s).\n\n");
  printf("Options:\n");
  printf("  -h, --help       display this help\n");
  printf("  -v, --version    display %s version\n", PACKAGE_NAME);
  printf("  -F, --force      overwrite output_file if it is non-empty\n");
  /* printf("  -I, --no-indent  do not indent acbf XML\n\n"); */
  printf("  -t, --title      comic title\n");
  printf("  -S, --series     series/sequence title, requires a series number as well\n");
  printf("  -N, --number     series number\n");
  printf("  -V, --ver        change document version, defaults to 1.0\n");
  printf("  -s, --source     document source, may occur more than once\n");
  printf("  -H, --history    log entry for document history\n");
  printf("  -f, --first      author first name\n");
  printf("  -m, --middle     author middle name\n");
  printf("  -l, --last       author last name\n");
  printf("  -n, --nick       author nickname/alias\n");
  printf("  -w, --homepage   author homepage\n");
  printf("  -n, --email      author email\n\n");
  printf("Environment:\n");
  printf("mkacbf will store document authorship tags if the following environmental variables are set:\n");
  printf("  ACBF_AUTHOR_FIRST_NAME\n");
  printf("  ACBF_AUTHOR_MIDDLE_NAME\n");
  printf("  ACBF_AUTHOR_LAST_NAME\n");
  printf("  ACBF_AUTHOR_NICKNAME\n");
  printf("  ACBF_AUTHOR_HOMEPAGE\n");
  printf("  ACBF_AUTHOR_EMAIL\n");
  printf("If none of these variables are set, author-nickname will be set to the login name of the current user.\n\n");
}

static void
printVersion ()
{
  //printf("%s %s\nACBF schema %s\n", progname, PACKAGE_VERSION, SCHEMA_NAMESPACE);
  printf("%s\n", PACKAGE_STRING);
  printf("Copyright (C) 2012 Emery Hemingway\n");
  printf("License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\n");
  printf("This is free software: you are free to change and redistribute it.\n");
  printf("There is NO WARRANTY, to the extent permitted by law.\n");
}

char *image_jpeg = "image/jpeg";
char *image_png = "image/png";
char *image_gif = "image/gif";

int
main(int argc, char **argv)
{
  /*
   * this initialize the library and check potential ABI mismatches
   * between the version it was compiled for and the actual shared
   * library used.
   */
  LIBXML_TEST_VERSION;

  char force_write = 0; 
  /* int indent = 1; */
  unsigned int i, j, k, length;
  xmlTextWriterPtr writer;

  char buffer[BUFFER_SIZE];
  char *output_filename, *title  = NULL, *sequence_title  = NULL, *sequence_number  = NULL,
    *author_first  = NULL, *author_middle = NULL, *author_last  = NULL, *author_nick  = NULL, *author_homepage  = NULL, *author_email  = NULL, 
    *version = NULL, *history_entry = NULL;
  time_t curtime;
  struct tm *ltime;

  unsigned int document_source_count = 0;
  char *document_sources[BUFFER_SIZE];

#ifdef HAS_LIBUUID
  uuid_t document_id;
#endif
  
  FILE *output_file, *image_file;

  size_t image_size;
  size_t image_read_length;
  char *image_buffer;  

  while (1)
  {
  
    i = getopt_long(argc, argv, "hvFIt:s:N:f:m:l:n:H:e:S:V:w:",
                    long_options, &j);
    if (i == -1)
      break;

    switch (i)
      {
      case 0:
        if (long_options[j].flag != 0)
          break;
        printf ("option %s", long_options[j].name);
        if (optarg)
          printf (" with arg %s", optarg);
        printf ("\n");
        break;

      case 'h':
        printUsage();
        return EXIT_FAILURE;

      case 'v':
        printVersion();
        return EXIT_FAILURE;

      case 'F':
        force_write = 1;
        break;

      /*
      case 'I':
        indent = 0;
        break; */

      case 't':
        title = optarg;
        break;

      case 'S':
        sequence_title = optarg;
        break;

      case 'N':
        sequence_number = optarg;
        break;

      case 'f':
        author_first = optarg;
        break;

      case 'm':
        author_middle = optarg;
        break;
       
      case 'l':
        author_last = optarg;
        break;

      case 'n':
        author_nick = optarg;
        break;

      case 'w':
        author_homepage = optarg;
        break;

      case 'e':
        author_email = optarg;
        break;

      case 's':
        document_sources[document_source_count] = optarg;
        document_source_count++;
        break;

      case 'V':
        version = optarg;
        break;

      case 'H':
        history_entry = optarg;
        break;

      case '?':
        break;

      default:
        printf("Unrecognized option -%c\n", i);
        return EXIT_FAILURE;
      }
  }

  argc--;

  if (optind > (argc - 2)) {
    /* Less than 2 pages to work with */
    printUsage();
    return EXIT_FAILURE;
  }

  /* Create an array of page filename pointers,
   * Create an array of page hrefs, these are the image basenames.
   * Create an array of page mime types, these are pointers to mime type strings.  
   */
  unsigned int page_count = argc - optind - 1;
  char *page_filenames[page_count];
  char *page_hrefs[page_count];
  char *page_mimes[page_count];

  j = 0;
  for (i = optind; i < argc; i++) {

    length = strlen(argv[i]);
    if (strcasecmp (argv[i] + length - 4, ".jpg") == 0 ||
        strcasecmp (argv[i] + length - 5, ".jpeg") == 0)
      page_mimes[j] = image_jpeg;
    
    else if (strcasecmp (argv[i] + length - 4, ".png") == 0)
      page_mimes[j] = image_png;

    else if (strcasecmp (argv[i] + length - 4, ".gif") == 0)
      page_mimes[j] = image_gif;

    else {
      fprintf (stderr, "Warning: %s has an unknown file extension, skipping...\n", argv[i]);
      page_count -= 1;
      continue;
    }

    page_filenames[j] = argv[i];
    page_hrefs[j] = argv[i];

    length = strlen(argv[i]) - 4;
    for (k = 0; k < length; k++) {
      if (argv[i][k] == '/') {
        page_hrefs[j] = &argv[i][k] + 1;
      }
    }
    j++;
  }
  
  output_filename = argv[argc];
  struct stat stat_buff;
  i = stat(output_filename, &stat_buff);

  if ((i == 0) && (stat_buff.st_size) && (force_write == 0)) 
    {
      printf("%s is non-empty, will not continue without --force\n", output_filename);
      return EXIT_FAILURE;
    }

  writer = xmlNewTextWriterFilename(output_filename, 0);
  if (writer == NULL) {
    printf("Error creating XML writer.\n");
    return EXIT_FAILURE;
  }
  xmlTextWriterSetIndent(writer, 1); /* indent); */

  xmlTextWriterStartDocument(writer, "1.0", "utf-8", NULL);
  // <ACBF>
  xmlTextWriterStartElement(writer, BAD_CAST "ACBF");
  xmlTextWriterWriteAttribute(writer, BAD_CAST "xmlns", BAD_CAST SCHEMA_NAMESPACE);
  
  // <meta-data>
  xmlTextWriterStartElement(writer, BAD_CAST "meta-data");

  // <book-info>
  xmlTextWriterStartElement(writer, BAD_CAST "book-info");
  // <author
  xmlTextWriterStartElement(writer, BAD_CAST "author");

  if (author_first)
    xmlTextWriterWriteElement(writer, BAD_CAST "first-name", BAD_CAST author_first);
  if (author_middle)
    xmlTextWriterWriteElement(writer, BAD_CAST "middle-name", BAD_CAST author_middle);
  if (author_last)
    xmlTextWriterWriteElement(writer, BAD_CAST "last-name", BAD_CAST author_last);
  if (author_nick)
    xmlTextWriterWriteElement(writer, BAD_CAST "nickname", BAD_CAST author_nick);
  if (author_homepage)
    xmlTextWriterWriteElement(writer, BAD_CAST "home-page", BAD_CAST author_homepage);
  if (author_email)
    xmlTextWriterWriteElement(writer, BAD_CAST "email", BAD_CAST author_email);
  xmlTextWriterEndElement(writer);
  // </author>

  if (title)
    // <title>
    xmlTextWriterWriteElement(writer, BAD_CAST "book-title", BAD_CAST title);
    // </title>

  // <coverpage>
  xmlTextWriterStartElement(writer, BAD_CAST "coverpage");
  // <image>
  xmlTextWriterStartElement(writer, BAD_CAST "image");
  strcpy (buffer, "#");
  strcat (buffer, page_hrefs[0]);
  xmlTextWriterWriteAttribute(writer, BAD_CAST "href", BAD_CAST buffer);
  xmlTextWriterEndElement(writer);
  // </image>
  xmlTextWriterEndElement(writer);
  // </coverpage>

  // <sequence>
  if ( (sequence_title) || (sequence_number) ) { 
    if ( (sequence_title) && (sequence_number) == 0) {
        fprintf(stderr, "Error: Missing a series title or number.");
        return(EXIT_FAILURE);
      }

    xmlTextWriterStartElement(writer, BAD_CAST "sequence");
    xmlTextWriterWriteAttribute(writer, BAD_CAST "title", BAD_CAST sequence_title);
    xmlTextWriterWriteString(writer, BAD_CAST sequence_number);
    xmlTextWriterEndElement(writer);
  }

  // </sequence>

  xmlTextWriterEndElement(writer);
  // </bookinfo>

  // <publish_info>
  xmlTextWriterWriteElement(writer, BAD_CAST "publish-info", NULL);
  xmlTextWriterEndElement(writer);
  // </publish_info>

  // <document-info>
  xmlTextWriterStartElement(writer, BAD_CAST "document-info");

  // <author>
  xmlTextWriterStartElement(writer, BAD_CAST "author");
  i = 0;

  author_first = getenv("ACBF_AUTHOR_FIRST_NAME");
  if (author_first) {
    xmlTextWriterWriteElement(writer, BAD_CAST "first-name", BAD_CAST author_first);
    i++;
  }

  author_middle = getenv("ACBF_AUTHOR_MIDDLE_NAME");
  if (author_middle) {
    xmlTextWriterWriteElement(writer, BAD_CAST "middle-name", BAD_CAST author_middle);
    i++;
  }

  author_last = getenv("ACBF_AUTHOR_LAST_NAME");
  if (author_last) {
    xmlTextWriterWriteElement(writer, BAD_CAST "last-name", BAD_CAST author_last);
    i++;
  }

  author_nick = getenv("ACBF_AUTHOR_NICKNAME");
  if (author_nick) {
    xmlTextWriterWriteElement(writer, BAD_CAST "nickname", BAD_CAST author_nick);
    i++;
  }

  author_homepage = getenv("ACBF_AUTHOR_HOMEPAGE");
  if (author_homepage) {
    xmlTextWriterWriteElement(writer, BAD_CAST "home-page", BAD_CAST author_homepage);
    i++;
  }

  author_email = getenv("ACBF_AUTHOR_EMAIL");
  if (author_email) {
    xmlTextWriterWriteElement(writer, BAD_CAST "email", BAD_CAST author_email);
    i++;
  }
         
  if (i == 0) {
    author_nick = getenv("LOGNAME");
    xmlTextWriterWriteElement(writer, BAD_CAST "nickname", BAD_CAST author_nick);
  }
  xmlTextWriterEndElement(writer);
  // </author>

  // <creation-date>
  curtime = time (NULL);
  ltime = localtime (&curtime);
  /* Date string in current locale format.*/
  strftime (buffer, BUFFER_SIZE, "%x", ltime);
  xmlTextWriterWriteElement(writer, BAD_CAST "creation-date", 
                            BAD_CAST buffer);
  strftime (buffer, BUFFER_SIZE, "%F", ltime);
  xmlTextWriterWriteAttribute(writer, BAD_CAST "value", BAD_CAST buffer);
  // </creation-date>

  // <source>
  if (document_source_count > 0) {
    xmlTextWriterStartElement(writer, BAD_CAST "source");
    for (i = 0; i < document_source_count; i++) {
      xmlTextWriterWriteElement(writer, BAD_CAST "p", BAD_CAST document_sources[i]);
    }
    xmlTextWriterEndElement(writer);
  }
  // </source>

  // <version>
  if (version == NULL)
    version = "1.0";
  xmlTextWriterWriteElement(writer, BAD_CAST "version", BAD_CAST version);  
  // </version>

  // <id>
#ifdef HAS_LIBUUID
  uuid_generate(document_id);
  uuid_unparse(document_id, buffer);
  xmlTextWriterWriteElement(writer, BAD_CAST "id", BAD_CAST buffer);
#else
  srand( (unsigned int) curtime);
  xmlTextWriterWriteFormatElement(writer, BAD_CAST "id", "%d", rand());
#endif
  // </id>

  // <history>
  xmlTextWriterStartElement(writer, BAD_CAST "history");
  if (history_entry == NULL)
    {
      sprintf(buffer, "%s ACBF created with %s.", version, PACKAGE_STRING);
    }
  else
    {
      sprintf(buffer, "%s %s.", version, history_entry);
    }

  xmlTextWriterWriteElement(writer, BAD_CAST "p", BAD_CAST buffer);
  xmlTextWriterEndElement(writer);
  // <history>

  xmlTextWriterEndElement(writer);
  // </document-info>
  
  xmlTextWriterEndElement(writer);
  // </meta-data>

  // <body>
  strcpy (buffer, "#");
  xmlTextWriterStartElement(writer, BAD_CAST "body");
  /* skip the cover page, that does not go in body */
  for (i = 1; i <= page_count; i++) {
    // <page>
    xmlTextWriterStartElement(writer, BAD_CAST "page");
    xmlTextWriterStartElement(writer, BAD_CAST "image");
    strcpy(buffer + 1, page_hrefs[i]);
    xmlTextWriterWriteAttribute(writer, BAD_CAST "href", buffer);
    xmlTextWriterEndElement(writer);
    xmlTextWriterEndElement(writer);
    // </page>
  }

  xmlTextWriterEndElement(writer);
  // </body>

  // <data>
  xmlTextWriterStartElement(writer, BAD_CAST "data");
  for (i = 0; i <= page_count; i++) 
    {
      image_file = fopen(page_filenames[i], "r");
      if (image_file == NULL) {
        fprintf (stderr, "Error: Could not open %s: %s\n", page_filenames[i], strerror(errno));
        return(EXIT_FAILURE);
      }

      // <binary>
      xmlTextWriterStartElement(writer, BAD_CAST "binary");
      xmlTextWriterWriteAttribute(writer, BAD_CAST "id", BAD_CAST page_hrefs[i]);
      xmlTextWriterWriteAttribute(writer, BAD_CAST "content-type", BAD_CAST page_mimes[i]);
      
      fseek(image_file, 0, SEEK_END);
      image_size = ftell(image_file);
      rewind(image_file);

      image_buffer = (char*) malloc(sizeof(char) * image_size);
      if (image_buffer == NULL)
        {
          fprintf(stderr, "Error: could not allocate memory.\n");
          return(EXIT_FAILURE);
        }
      
      image_read_length = fread(image_buffer, sizeof(char), image_size, image_file);
      xmlTextWriterWriteBase64(writer, image_buffer, 0, image_read_length);

      xmlTextWriterEndElement(writer);
      xmlTextWriterFlush(writer);

      free(image_buffer);
      // </binary>
    }

  xmlTextWriterEndDocument(writer);
  // </data>
  // </ACBF>
  xmlTextWriterFlush(writer);

  return EXIT_SUCCESS;
}
