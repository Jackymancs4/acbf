1. copy whole program structure (Viewer/Editor directory) to acbf-viewer/acbf-editor directory
2. update debian/changelog file with the newest version details
3. run "debuild -S" in acbf-viewer/acbf-editor directory
4. run "dput ppa:acbf-development-team/acbf acbf-viewer_1.02_source.changes" outside of acbf-viewer/acbf-editor directory
5. check launchpad if package was built successfully

