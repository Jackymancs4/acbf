#!/bin/bash

VERSION_webp=${VERSION_webp:-0.5.0}
DEPS_webp=(png jpeg python)
URL_webp=http://downloads.webmproject.org/releases/webp/libwebp-0.5.0.tar.gz
MD5_webp=ba81eb9bf23e3c69a2f5cc8dcdb5938f
BUILD_webp=$BUILD_PATH/webp/$(get_directory $URL_webp)
RECIPE_webp=$RECIPES_PATH/webp

# function called for preparing source code if needed
# (you can apply patch etc here.)
function prebuild_webp() {
    try cp -a $RECIPE_webp/patches/cpu-features.h $BUILD_webp/src/dsp/
}

function build_webp() {
    cd $BUILD_webp
    push_arm
    export CFLAGS="$CFLAGS -I$JNI_PATH/png -I$JNI_PATH/jpeg"
    try ./configure --build=i686-pc-linux-gnu --host=arm-linux-androideabi --prefix=$BUILD_webp --enable-shared
    try make -j5
    pop_arm

    try cp $BUILD_webp/src/.libs/libwebp.so $LIBS_PATH
}

# function called after all the compile have been done
function postbuild_webp() {
    true
}


