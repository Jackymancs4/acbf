#!/bin/bash

VERSION_patool=1.12
DEPS_patool=(python)
URL_patool=http://pypi.python.org/packages/source/p/patool/patool-$VERSION_patool.tar.gz
MD5_patool=32a764139a018c6bc497b25630513a33
BUILD_patool=$BUILD_PATH/patool/$(get_directory $URL_patool)
RECIPE_patool=$RECIPES_PATH/patool

function prebuild_patool() {
	cd $BUILD_patool
        ls

	# check marker in our source build
	if [ -f .patched ]; then
		# no patch needed
		return
	fi

	try patch setup.py $RECIPE_patool/setup.patch

	# everything done, touch the marker !
	touch .patched
}

function shouldbuild_patool() {
	if [ -d "$BUILD_PATH/python-install/lib/python2.7/site-packages/patool" ]; then
		DO_BUILD=0
	fi
}

function build_patool() {
	cd $BUILD_patool

	push_arm

	try $HOSTPYTHON setup.py install

	pop_arm
}

function postbuild_patool() {
	true
}

